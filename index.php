<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./node_modules/material-icons/iconfont/material-icons.css">
    <link rel="stylesheet" href="./node_modules/materialize-css/dist/css/materialize.min.css">
    <link rel="stylesheet" href="./node_modules/sweetalert2/dist/sweetalert2.css">
    <script src="./node_modules/materialize-css/dist/js/materialize.min.js"></script>
    <link rel="stylesheet" href="./public/css/style.css">
    <title>Cadastro de Produtos</title>
</head>
<body>
    <div class="row" id="root">
        <div class="col s12 l6 push-l3">
            <form v-on:submit.prevent="salvar()">

                <div class="input-field col s12">
                    <div class="file-field input-field">
                        <div class="btn">
                          <span>Foto</span>
                          <input type="file" name="arquivo" id="arquivo">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">face</i>
                    <input type="text" v-model="dados.nome" name="nome" id="nome">
                    <label for="nome">nome</label>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">grade</i>
                    <input type="date" v-model="dados.nascimento" name="nascimento" id="nascimento">                    
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">fingerprint</i>
                    <input type="text" v-model="dados.identidade" name="identidade" id="identidade">
                    <label for="identidade">identidade</label>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">email</i>
                    <input type="text" v-model="dados.email" name="email" id="email">
                    <label for="email">email</label>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">phone</i>
                    <input type="tel" v-model="dados.telefone" name="telefone" id="telefone">
                    <label for="telefone">telefone</label>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">markunread_mailbox</i>
                    <input type="text" v-model="dados.cep" name="cep" id="cep">
                    <label for="cep">cep</label>
                </div>

                <div class="input-field col s12">
                    <button type="submit" class="btn right">
                    salvar<i class="material-icons right">send</i>
                    </button>
                </div>

            </form>
        </div>
    </div>


    
    <script src="./node_modules/vue/dist/vue.js"></script>
    <script src="./node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="./public/js/script.js"></script>
</body>
</html>

