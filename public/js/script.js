let root = new Vue({
    el:"#root",
    data:{
        dados:{
            arquivo:null,
            nome:null,
            nascimento:null,
            identidade:null,
            email:null,
            telefone:null,
            cep:null
        }
        
    },
    mounted(){
        M.AutoInit();
    },
    methods:{
        
        salvar(){

            var formData = new FormData();
            formData.append("arquivo",document.getElementById("arquivo").files[0])
            formData.append("nome",this.dados.nome)
            formData.append("nascimento",this.dados.nascimento)
            formData.append("identidade",this.dados.identidade)
            formData.append("email",this.dados.email)
            formData.append("telefone",this.dados.telefone)
            formData.append("cep",this.dados.cep)

            fetch('http://localhost:3000/upload.php',{
                method:'POST',
                headers:{
               
                },
                body:formData
            })
            .then(res=>res.json())
            .then(data=>{
                Swal.fire({
                    icon:"success",
                    title:"Sucesso :)",
                    text:JSON.stringify(data)
                })
            })
            .catch(err=>{
                Swal.fire({
                    icon:"error",
                    title:"Erro :(",
                    text:`Desculpe houve um erro: ${err}`
                })
            })
        }

    }
})