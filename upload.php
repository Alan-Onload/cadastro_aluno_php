<?php
header("Content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");

require_once __DIR__ . "/model/Aluno.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $pasta = "./uploads/";

    $arquivo = $_FILES["arquivo"];

    $nome = $arquivo['name'];

    $tamanho = $arquivo['size'];

    $destino = $pasta . basename($nome);

    $extensao = strtolower(pathinfo($destino, PATHINFO_EXTENSION));

    $processar = 1;

    $dados = ["nome" => $nome, "tamanho" => $tamanho, "extensão" => $extensao, "nota"=>"sucesso :)"];

    if ($tamanho  > 1000000) {
        $processar = 0;
        $dados['nota'] = "tamanho inválido :(";
    }

    if ($extensao != "jpg" && $extensao != "png" && $extensao != "jpeg") {
        $processar = 0;
        $dados['nota'] = "extensao inválida :(";

    }


    if ($processar == 1) {

        move_uploaded_file($arquivo["tmp_name"], $destino);
        $al = new Cliente();
        $al->setData(date('d/m/Y'));
        $al->setFoto($nome);
        $al->setNome($_POST['nome']);
        $al->setNascimento($_POST['nascimento']);
        $al->setIdentidade($_POST['identidade']);
        $al->setEmail($_POST['email']);
        $al->setTelefone($_POST['telefone']);
        $al->setCep($_POST['cep']);
        echo $al->ToJson();
       
    }
}
